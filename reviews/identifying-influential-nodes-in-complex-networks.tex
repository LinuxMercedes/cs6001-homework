\documentclass{article}

\author{Nathan Jarus}
\title{Identifying Influential Nodes in Complex Networks: A Critical Review}
\date{\today}

\begin{document}
\maketitle

\section{Paper Summary}

% Problem background - Slow metrics or worthless
In large complex networks, particularly social networks, common network centrality metrics are either prohibitively expensive to calculate or do not provide meaningful results. 
Closeness and Betweenness centrality take time at least proportional to the square of the number of vertices, and Degree centrality does not correlate strongly with node influence models. 

This paper develops a new centrality metric, Local Centrality. 
It is designed to run in linear time with respect to the number of vertices on sparse networks. 
As well, it performs on par with Closeness centrality as far as metric quality is concerned. 

The Local centrality of a node is the sum of the number of nearest and next-nearest neighbors of the node's neighbors. 
Mathematically, 

$$
C_L(v) = \sum_{u \in \Gamma(v)}\sum_{w \in \Gamma(u)} N(w)
$$

where $\Gamma(v)$ is the set of the nearest neighbors of node $v$, and $N(v)$ is the number of nearest and next-nearest neighbors of node $v$. 

The relevance of the various centrality metrics was tested with the Susceptible-Infected-Recovered (SIR) method, which draws inspiration from epidemeology. 
Each node can either be susceptible to being infected, infected, or recovered and therefore immune from being reinfected. 
At each step of the simulation, each infected node randomly infects one of its susceptible neighbors and can recover from being infected with probability $1/\langle k \rangle$, where $\langle k \rangle$ is the average degree of the network. 
Initially, one node is chosen to be infected. 
The SIR model indicates how ideas spread in a network; therefore, the number of infected and recovered nodes after $t$  steps, denoted $F(t)$ indicate the centrality of the initially infected node in the network. 

For testing the centrality metrics, the authors chose four large networks: the MSN Spaces blog communication network, the co-authorship network of scientists studying networks, the internet router topology, and the network of emails between members of the University Rovira i Virgili. 
Local centrality and Closeness centrality correlated with $F(t)$ approximately equally with $t=10$ on the Blogs, NetScience, and Email networks. 
No metric performed well on the internet router dataset, which indicatest that the internet router architecture does not mirror a social network well. 

Local centrality also performed about as well or better than Closeness centrality when considering how the metric ranks nodes compared to how they rank with respect to $F(t)$. 
Ideally, the average $F(t)$ of the top-$k$ nodes should decrease as $k$ increases, and in all three social networks, Local and Closeness centrality have this general behavior. 

Finally, Local centrality is correlated with Degree, Betweenness, and Closeness centrality values. 
In all networks, it correlates most strongly with Closeness centrality. 

Overall, the authors develop a centrality metric that performs similarly to Closeness centrality in social networks as defined by an SIR model, but has lower computational complexity to calculate. 

\section{Comments on the Paper}

The paper was well written and the analysis was convincing, although only for social network applications. 

The authors focus on $F(t)$ with $t=10$, but it would be instructive to see results for $t=5$, $t=20$, and $t=t_c$, where $t_c$ is the point at which $F(t)$ becomes stable. 
I would expect Local centrality to perform better with small $t$ and Closeness centrality to perform better with larger $t$ since the $F(t)$ for small $t$ is dependent directly on a node's neighbors and next neighbors, but with large $t$, $F(t)$ is more dependent on how quickly non-neighboring nodes can be reached from the initial node. 

As well, the authors exclusively compare Degree and Local centrality in certain other tests, which is not particularly informative since Degree centrality performs poorly compared to all other centrality metrics considered in the paper. 

\section{Future Work}

The authors mention that, at least in the Blogs dataset, when Local centrality is held constant, Degree centrality is an inverse predictor of node importance. 
I would be interested to see the performance of a metric $C_{L/D} = \frac{C_L}{C_D}$, just based on this empirical observation. 
However, it is possible that Degree and Local centrality are measuring too much of the same importance, especially in the Email dataset, and therefore dividing the two would result in a metric that performs very poorly. 

One other concern with Local centrality is that it counts nodes reachable from multiple neighbors multiple times. 
While this is acceptable since this does increase the risk of that node being infected, it is possible that the rate of risk increase is not linear with the number of ways that node can be reached from the node we are calculating the Local centrality of. 
In particular, I suspect that the fact that Recovered nodes are not able to be infected again may cause this nonlinear relationship. 
A possible solution to this issue would be to count the number of times each node is reached from the node whose centrality we are calculating and `discount' any nodes reachable more than once according to some polynomial or logarithmic function. 


\end{document}

