import json
import sys
import matplotlib.pyplot as plt
import numpy as np

data = {}
for fname in sys.argv[1:]:
	with open(fname) as jsonfile:
		data = (json.load(jsonfile))
	
colors = ['blue', 'green', 'red'] # who needs more than three colors really

def plot(data, key, label, graph):
	xvals = sorted(data.keys(), key=lambda x: float(x))
	yvals = [float(data[xval][key]) for xval in xvals]
	index = np.arange(len(xvals))
	plt.bar(index, yvals, width = 1)

	plt.xticks(index + 0.5, [xval[0:4] for xval in xvals], rotation=-60)
	plt.ylabel(label)
	plt.savefig('img/' + graph + '-' + key + '.pdf')
	plt.clf()

def histogram(key, label, file):
	yvals = [d[key] for d in data]
	labels = [d['title'] for d in data]
	for i,y in enumerate(yvals):
		plt.bar(i, y, 0.75, facecolor=colors[i], alpha=0.75)

	plt.ylabel(label)
	plt.legend(labels)
	plt.gca().xaxis.set_major_locator(plt.NullLocator())

	plt.savefig('img/' + file + '.pdf')
	plt.clf()

def distribution(key, label, file):
	labels = [d['title'] for d in data]

	for d,color in zip(data, colors):
		degrees = [n[key] for n in d['metrics']]
		max_deg = float(max(degrees))
		x = sorted([d/max_deg for d in degrees], reverse=True)

		plt.plot(np.arange(len(x)), x, color)

	plt.ylabel(label)
	plt.legend(labels)

	plt.savefig('img/' + file + '.pdf')
	plt.clf()

keys = ['erdos-renyi', 'barabasi-albert']

linevals = {
		'erdos-renyi' : [0.1001, 0.9001], # hahaha floats
		'barabasi-albert': [50, 200]
		}

for key in keys:
	plot(data[key], 'edges', 'Number of Edges', key)
	plot(data[key], 'avg degree', 'Average Degree', key)
	plot(data[key], 'largest component', 'Size of Largest Component', key)
	plot(data[key], 'avg path', 'Average Path Length', key)
	plot(data[key], 'diameter', 'Network Diameter', key)

	#lines = sorted(data[key].keys(), key=lambda x: float(x))
	lines = linevals[key]

	lvals = []
	for line in lines:
		line = max(filter(lambda x: float(x) <= line, data[key].keys()), key=lambda x:float(x))
		lvals.append(line)
		dist = {}
		for node in data[key][line]['metrics']:
			if node['degree'] in dist:
				dist[node['degree']] += 1
			else:
				dist[node['degree']] = 1

		xvals = sorted(dist.keys())
		yvals = [dist[x]/500.0 for x in xvals]
		plt.plot(xvals, yvals)

	plt.ylabel("P(Degree)")
	plt.xlabel("Degree")
	plt.legend(lvals)
	plt.savefig('img/' + key + '-degree distribution.pdf')
	plt.clf()


