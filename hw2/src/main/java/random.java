package org.gephi.toolkit.demos;

import java.io.*;
import java.util.*;
import java.util.concurrent.TimeUnit;

import org.gephi.graph.api.*;
import org.gephi.project.api.ProjectController;
import org.gephi.project.api.Workspace;
import org.openide.util.Lookup;

import org.gephi.io.importer.api.*;
import org.gephi.io.processor.plugin.DefaultProcessor;
import org.gephi.io.importer.spi .*;

import org.gephi.statistics.plugin.*;
import org.gephi.data.attributes.api.*;
import org.gephi.io.exporter.api.*;
import org.gephi.statistics.plugin.*;

import org.gephi.ranking.api.*;
import org.gephi.ranking.plugin.transformer.*;

import org.gephi.statistics.plugin.EigenvectorCentrality;

import org.gephi.layout.*;
import org.gephi.layout.plugin.*;
import org.gephi.layout.plugin.forceAtlas2.*;

import org.json.JSONWriter;

public class random{

  public random(){

  }

  public static void main(String args[]){

    //Gephi initialization
    ProjectController pc = Lookup.getDefault().lookup(ProjectController.class);
    pc.newProject();
    Workspace workspace = pc.getCurrentWorkspace();

    GraphModel graphModel = Lookup.getDefault().lookup(GraphController.class).getModel();
    AttributeModel attributeModel = Lookup.getDefault().lookup(AttributeController.class).getModel();
    Graph graph = graphModel.getGraph();

    // Problem A
    graphModel.getGraph().clear();
    erdos_renyi(graphModel, 50, 0.5);
    varySize();
    forceAtlas(graphModel);
    exportPDF("erdos-renyi");

    graphModel.getGraph().clear();
    barabasi_albert(graphModel, 50, 10);
    varySize();
    forceAtlas(graphModel);
    exportPDF("barabasi-albert");

    // Problem B

    FileWriter jsonfile;
    try {
      jsonfile = new FileWriter("hw2.json");
    } catch (Exception ex) {
      ex.printStackTrace();
      return;
    }
    JSONWriter json = new JSONWriter(jsonfile);

    json.object();
    json.key("erdos-renyi").object();

    for(double i = 0; i <= 1.0; i  += 0.025){
      json.key(String.valueOf(i)).object();

      graph.clear();
      erdos_renyi(graphModel, 500, i);

      json.key("edges").value((long) graph.getEdgeCount());

      //calculate the betweeness and closeness centralities
      GraphDistance graphDistance = new GraphDistance();
      ConnectedComponents cc = new ConnectedComponents();
      cc.execute(graphModel, attributeModel);

      graphDistance.execute(graphModel,attributeModel);

      long degrees = 0;
      json.key("metrics").array();
      for(Node node : graph.getNodes()) {
        json.object();
        json.key("node").value(node.getNodeData().getLabel());
        json.key("degree").value((long) graph.getDegree(node));		

        degrees += graph.getDegree(node);
        json.endObject();
      }
      json.endArray();

      json.key("avg degree").value(((double)degrees)/graph.getNodeCount());
      json.key("largest component").value(cc.getComponentsSize()[cc.getGiantComponent()]);
      json.key("avg path").value(String.valueOf(graphDistance.getPathLength()));
      json.key("diameter").value(graphDistance.getDiameter());

      json.endObject();
    }

    json.endObject();

    json.key("barabasi-albert").object();

    for(int i = 0; i <= 250; i  += 10){
      json.key(String.valueOf(i)).object();

      graph.clear();
      barabasi_albert(graphModel, 500, i);

      json.key("edges").value((long) graph.getEdgeCount());

      //calculate the betweeness and closeness centralities
      GraphDistance graphDistance = new GraphDistance();
      ConnectedComponents cc = new ConnectedComponents();
      cc.execute(graphModel, attributeModel);

      graphDistance.execute(graphModel,attributeModel);

      long degrees = 0;
      json.key("metrics").array();
      for(Node node : graph.getNodes()) {
        json.object();
        json.key("node").value(node.getNodeData().getLabel());
        json.key("degree").value((long) graph.getDegree(node));		

        degrees += graph.getDegree(node);
        json.endObject();
      }
      json.endArray();

      json.key("avg degree").value(((double)degrees)/graph.getNodeCount());
      json.key("largest component").value(cc.getComponentsSize()[cc.getGiantComponent()]);
      json.key("avg path").value(String.valueOf(graphDistance.getPathLength()));
      json.key("diameter").value(graphDistance.getDiameter());

      json.endObject();
    }

    json.endObject();
    json.endObject();

    try {
      jsonfile.close();
    } catch (Exception ex) {
      ex.printStackTrace();
      return;
    }

  }


  static void erdos_renyi(GraphModel graphModel, int n, double p) {
    Random rand = new Random();
    GraphFactory f = graphModel.factory();
    Graph g = graphModel.getGraph();

    for(int i = 0; i < n; i++) {
      Node node = f.newNode();
      List<Edge> edges = new LinkedList<Edge>();

      for(Node gn:g.getNodes()) {
        if(rand.nextDouble() < p) {
          edges.add(f.newEdge(node, gn));
        }
      }

      g.addNode(node);

      for(Edge e:edges) {
        g.addEdge(e);
      }
    }
  }

  static void barabasi_albert(GraphModel graphModel, int n, int c) {
    Random rand = new Random();
    GraphFactory f = graphModel.factory();
    Graph g = graphModel.getGraph();

    //Construct fully-connected graph of c nodes
    for(int i = 0; i < c; i++) {
      List<Edge> edges = new LinkedList<Edge>();
      Node node = f.newNode();
      for(Node gn:g.getNodes()) {
        edges.add(f.newEdge(node,gn));
      }

      g.addNode(node);

      for(Edge e:edges) {
        g.addEdge(e);
      }
    }

    // Add n-c more edges 
    for(int i = c; i < n; i++) {
      Node node = f.newNode();
      List<Edge> edges = new LinkedList<Edge>();
      List<Node> nodes = new ArrayList<Node>(Arrays.asList(g.getNodes().toArray()));

      for(int j = 0; j < c; j++) {
        int k = rand.nextInt(nodes.size());
        edges.add(f.newEdge(node, nodes.get(k)));
        nodes.remove(k);
      }

      g.addNode(node);
      for(Edge e:edges) {
        g.addEdge(e);
      }
    }
  }
        
  static void forceAtlas(GraphModel graphModel) {
    ForceAtlas2 layout = new ForceAtlas2Builder().buildLayout();
    layout.setGraphModel(graphModel);
    layout.initAlgo();
    layout.resetPropertiesValues();
    layout.setAdjustSizes(true);
    layout.setBarnesHutOptimize(true);
    layout.setScalingRatio(200.0);
    layout.setThreadsCount(2);

    for (int i = 0; i < 10000 && layout.canAlgo(); i++) {
      layout.goAlgo();
    }
    layout.endAlgo();
  }

  static void varySize( ) {
    RankingController rankingController = Lookup.getDefault().lookup(RankingController.class);
    Ranking ranking = rankingController.getModel().getRanking(Ranking.NODE_ELEMENT, Ranking.DEGREE_RANKING);
    AbstractSizeTransformer sizeTransformer = (AbstractSizeTransformer) rankingController.getModel().getTransformer(Ranking.NODE_ELEMENT, Transformer.RENDERABLE_SIZE);
    sizeTransformer.setMinSize(3);
    sizeTransformer.setMaxSize(20);
    rankingController.transform(ranking,sizeTransformer);
  }

  static void exportPDF(String fileName){

    ExportController ec = Lookup.getDefault().lookup(ExportController.class);

    try {

      ec.exportFile(new File(fileName+".pdf"));
    } catch (IOException ex) {
      ex.printStackTrace();
      return;
    }

  }


  static void exportGraph(Graph graph, String filename){

    try{

      ExportController ec = Lookup.getDefault().lookup(ExportController.class);
      ec.exportFile(new File(filename+".gexf"));

    }catch(IOException ex){

    }
  }
}