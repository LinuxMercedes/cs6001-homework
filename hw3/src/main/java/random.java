package org.gephi.toolkit.demos;

import java.io.*;
import java.util.*;
import java.util.concurrent.TimeUnit;

import org.gephi.graph.api.*;
import org.gephi.project.api.ProjectController;
import org.gephi.project.api.Workspace;
import org.openide.util.Lookup;
import org.gephi.data.attributes.api.*;

import org.gephi.io.importer.api.*;
import org.gephi.io.processor.plugin.DefaultProcessor;
import org.gephi.io.importer.spi .*;

import org.gephi.statistics.plugin.*;
import org.gephi.data.attributes.api.*;
import org.gephi.io.exporter.api.*;
import org.gephi.statistics.plugin.*;

import org.gephi.ranking.api.*;
import org.gephi.ranking.plugin.transformer.*;

import org.gephi.statistics.plugin.EigenvectorCentrality;

import org.gephi.layout.*;
import org.gephi.layout.plugin.*;
import org.gephi.layout.plugin.forceAtlas2.*;

import org.json.JSONWriter;

import java.util.Collections;

public class random{

  public random(){

  }

  public static void main(String args[]){

    //Gephi initialization
    ProjectController pc = Lookup.getDefault().lookup(ProjectController.class);
    pc.newProject();
    Workspace workspace = pc.getCurrentWorkspace();

    GraphModel graphModel = Lookup.getDefault().lookup(GraphController.class).getModel();
    AttributeModel attributeModel = Lookup.getDefault().lookup(AttributeController.class).getModel();
    Graph graph = graphModel.getGraph();

    FileWriter jsonfile;
    try {
      jsonfile = new FileWriter("hw3.json");
    } catch (Exception ex) {
      ex.printStackTrace();
      return;
    }
    JSONWriter json = new JSONWriter(jsonfile);
    json.object();

    int iterations = 1000;
    // Problem A.a
    
    json.key("robustness").object();
    json.key("phi").object();

    json.key("erdos-renyi").object();
    for(double phi = 0.01; phi <= 1.0; phi += 0.01) {
      json.key(String.valueOf(phi)).array();
      
      for(int i = 0; i < iterations; i++) {
        graphModel.getGraph().clear();
        erdos_renyi(graphModel, 50, 0.5);
        remove_nodes(graphModel, phi);

        ConnectedComponents cc = new ConnectedComponents();
        cc.execute(graphModel, attributeModel);
        if(cc.getGiantComponent() == -1) {
          json.value(0);
        } else {
          json.value(cc.getComponentsSize()[cc.getGiantComponent()]);
        }
      }

      json.endArray();
    }
    json.endObject();

    json.key("barabasi-albert").object();

    for(double phi = 0.01; phi <= 1.0; phi += 0.1) {
      json.key(String.valueOf(phi)).array();
      
      for(int i = 0; i < iterations; i++) {
        graphModel.getGraph().clear();
        barabasi_albert(graphModel, 50, 10);
        remove_nodes(graphModel, phi);

        ConnectedComponents cc = new ConnectedComponents();
        cc.execute(graphModel, attributeModel);
        if(cc.getGiantComponent() == -1) {
          json.value(0);
        } else {
          json.value(cc.getComponentsSize()[cc.getGiantComponent()]);
        }
      }

      json.endArray();
    }
    json.endObject();

    json.endObject(); //phi

    // Problem A.b

    json.key("edges").object();

    double phi = 0.5;

    json.key("erdos-renyi").object();
    for(double p = 0.1; p <= 1.0; p += 0.01) {
      json.key(String.valueOf(p)).array();
      
      for(int i = 0; i < iterations; i++) {
        graphModel.getGraph().clear();
        erdos_renyi(graphModel, 50, p);
        remove_nodes(graphModel, phi);

        ConnectedComponents cc = new ConnectedComponents();
        cc.execute(graphModel, attributeModel);
        if(cc.getGiantComponent() == -1) {
          json.value(0);
        } else {
          json.value(cc.getComponentsSize()[cc.getGiantComponent()]);
        }
      }

      json.endArray();
    }
    json.endObject();

    json.key("barabasi-albert").object();

    for(int c = 1; c <= 10; c += 1) {
      json.key(String.valueOf(c)).array();
      
      for(int i = 0; i < iterations; i++) {
        graphModel.getGraph().clear();
        barabasi_albert(graphModel, 50, c);
        remove_nodes(graphModel, phi);

        ConnectedComponents cc = new ConnectedComponents();
        cc.execute(graphModel, attributeModel);
        if(cc.getGiantComponent() == -1) {
          json.value(0);
        } else {
          json.value(cc.getComponentsSize()[cc.getGiantComponent()]);
        }
      }

      json.endArray();
    }
    json.endObject();

    json.endObject(); //edges


    json.endObject(); //robustness

    System.out.println("B");
    // Problem B

    json.key("SI").object();

    // B.a
    json.key("beta").object();

    json.key("erdos-renyi").object();
    for(double beta = 0.1; beta <= 1.0; beta += 0.01) {
      json.key(String.valueOf(beta)).array();
      
      for(int i = 0; i < iterations; i++) {
        graphModel.getGraph().clear();
        erdos_renyi(graphModel, 50, 0.5);
        json.value(si(graphModel, beta));
      }

      json.endArray();
    }
    json.endObject();

    json.key("barabasi-albert").object();
    for(double beta = 0.1; beta <= 1.0; beta += 0.01) {
      json.key(String.valueOf(beta)).array();
      
      for(int i = 0; i < iterations; i++) {
        graphModel.getGraph().clear();
        barabasi_albert(graphModel, 50, 5);
        json.value(si(graphModel, beta));
      }

      json.endArray();
    }
    json.endObject();

    json.endObject(); //beta
    
    // B.b
    json.key("edges").object();

    double beta = 0.5;

    json.key("erdos-renyi").object();
    for(double p = 0.1; p <= 1.0; p += 0.01) {
      json.key(String.valueOf(p)).array();
      
      for(int i = 0; i < iterations; i++) {
        graphModel.getGraph().clear();
        erdos_renyi(graphModel, 50, p);
        json.value(si(graphModel, beta));
      }

      json.endArray();
    }
    json.endObject();

    json.key("barabasi-albert").object();
    for(int c = 1; c <= 10; c += 1) {
      json.key(String.valueOf(c)).array();
      
      for(int i = 0; i < iterations; i++) {
        graphModel.getGraph().clear();
        barabasi_albert(graphModel, 50, c);
        json.value(si(graphModel, beta));
      }

      json.endArray();
    }
    json.endObject();

    json.endObject(); //edges

    json.endObject(); //SI

    json.endObject();

    try {
      jsonfile.close();
    } catch (Exception ex) {
      ex.printStackTrace();
      return;
    }

  }

  static Set<Node> getGiantComponent(GraphModel graphModel) {
    AttributeModel attributeModel = Lookup.getDefault().lookup(AttributeController.class).getModel();
    ConnectedComponents cc = new ConnectedComponents();
    cc.execute(graphModel, attributeModel);

    int gc = cc.getGiantComponent();

    Set<Node> ret = new HashSet<Node>();

    for(Node n : graphModel.getGraph().getNodes()) {
      if(n.getAttributes().getValue(ConnectedComponents.WEAKLY) == gc) {
        ret.add(n);
      }
    }

    return ret;
  }

  static int si(GraphModel graphModel, double beta) {
    Random rand = new Random();
    Graph g = graphModel.getGraph();

    Set<Node> infected = new HashSet<Node>();
    // Pick an infected node UAR
    infected.add(g.getNodes().toArray()[rand.nextInt(g.getNodeCount())]);

    int infinity = 32768; // according to a student in the class I teach
    int time;

    Set<Node> gc = getGiantComponent(graphModel);
    gc.removeAll(infected);

    for(time = 0; gc.size() > 0 && time < infinity; time++) {
      Set<Node> sickos = new HashSet<Node>();

      for(Node i : infected) {
        for(Edge e : g.getEdges(i)) {
          Node n = g.getOpposite(i,e);
          if(!sickos.contains(n) && rand.nextDouble() < beta) {
            sickos.add(n);
          }
        }
      }

      infected.addAll(sickos);
      gc.removeAll(infected);
    }

    return time;
  }

  static void remove_nodes(GraphModel graphModel, double phi) {
    Graph g = graphModel.getGraph();
    List<Integer> indices = new ArrayList<Integer>();
    for(Node n : g.getNodes()) {
      indices.add(n.getId());
    }

    Collections.shuffle(indices);

    double max = g.getNodeCount()*phi;
    for(int i = 0; i <= max; i++) {
      g.removeNode(g.getNode(indices.get(i)));
    }
  }

  static void erdos_renyi(GraphModel graphModel, int n, double p) {
    Random rand = new Random();
    GraphFactory f = graphModel.factory();
    Graph g = graphModel.getGraph();

    for(int i = 0; i < n; i++) {
      Node node = f.newNode();
      List<Edge> edges = new LinkedList<Edge>();

      for(Node gn:g.getNodes()) {
        if(rand.nextDouble() < p) {
          edges.add(f.newEdge(node, gn));
        }
      }

      g.addNode(node);

      for(Edge e:edges) {
        g.addEdge(e);
      }
    }
  }

  static void barabasi_albert(GraphModel graphModel, int n, int c) {
    Random rand = new Random();
    GraphFactory f = graphModel.factory();
    Graph g = graphModel.getGraph();

    //Construct fully-connected graph of c nodes
    for(int i = 0; i < c; i++) {
      List<Edge> edges = new LinkedList<Edge>();
      Node node = f.newNode();
      for(Node gn:g.getNodes()) {
        edges.add(f.newEdge(node,gn));
      }

      g.addNode(node);

      for(Edge e:edges) {
        g.addEdge(e);
      }
    }

    // Add n-c more edges 
    for(int i = c; i < n; i++) {
      Node node = f.newNode();
      List<Edge> edges = new LinkedList<Edge>();
      List<Node> nodes = new ArrayList<Node>(Arrays.asList(g.getNodes().toArray()));

      for(int j = 0; j < c; j++) {
        int k = rand.nextInt(nodes.size());
        edges.add(f.newEdge(node, nodes.get(k)));
        nodes.remove(k);
      }

      g.addNode(node);
      for(Edge e:edges) {
        g.addEdge(e);
      }
    }
  }
        
  static void forceAtlas(GraphModel graphModel) {
    ForceAtlas2 layout = new ForceAtlas2Builder().buildLayout();
    layout.setGraphModel(graphModel);
    layout.initAlgo();
    layout.resetPropertiesValues();
    layout.setAdjustSizes(true);
    layout.setBarnesHutOptimize(true);
    layout.setScalingRatio(200.0);
    layout.setThreadsCount(2);

    for (int i = 0; i < 10000 && layout.canAlgo(); i++) {
      layout.goAlgo();
    }
    layout.endAlgo();
  }

  static void varySize( ) {
    RankingController rankingController = Lookup.getDefault().lookup(RankingController.class);
    Ranking ranking = rankingController.getModel().getRanking(Ranking.NODE_ELEMENT, Ranking.DEGREE_RANKING);
    AbstractSizeTransformer sizeTransformer = (AbstractSizeTransformer) rankingController.getModel().getTransformer(Ranking.NODE_ELEMENT, Transformer.RENDERABLE_SIZE);
    sizeTransformer.setMinSize(3);
    sizeTransformer.setMaxSize(20);
    rankingController.transform(ranking,sizeTransformer);
  }

  static void exportPDF(String fileName){

    ExportController ec = Lookup.getDefault().lookup(ExportController.class);

    try {

      ec.exportFile(new File(fileName+".pdf"));
    } catch (IOException ex) {
      ex.printStackTrace();
      return;
    }

  }


  static void exportGraph(Graph graph, String filename){

    try{

      ExportController ec = Lookup.getDefault().lookup(ExportController.class);
      ec.exportFile(new File(filename+".gexf"));

    }catch(IOException ex){

    }
  }
}