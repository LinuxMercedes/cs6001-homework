import json
import sys
import matplotlib.pyplot as plt
import numpy as np

data = {}
for fname in sys.argv[1:]:
	with open(fname) as jsonfile:
		data = (json.load(jsonfile))
	
colors = ['blue', 'green', 'red'] # who needs more than three colors really

def plot(data, ylabel, xlabel, graph):
	xvals = sorted(data.keys(), key=lambda x: float(x))
	cdata = dict([(xval, [x for x in data[xval] if x < 32768]) for xval in xvals])
	yvals = [sum(cdata[xval])/float(len(cdata[xval])) for xval in xvals]

	missing = [len(data[xval]) - len(cdata[xval]) for xval in xvals]

	xvals = [float(x) for x in xvals]
	plt.plot(xvals, yvals)
	plt.xlabel(xlabel)

	if sum(missing) > 0:
		plt.plot(xvals, missing)
		plt.legend([ylabel, 'Number of infinite datapoints'])
	else:
		plt.ylabel(ylabel)#, rotation='horizontal')

	plt.savefig('img/' + graph + '.pdf')
	plt.clf()

def twoplot(data, ylabel, xlabel, graph):
	baxvals = sorted(data['barabasi-albert'].keys(), key=lambda x: float(x))
	bayvals = [sum(data['barabasi-albert'][xval])/float(len(data['barabasi-albert'][xval])) for xval in baxvals]

	erxvals = sorted(data['erdos-renyi'].keys(), key=lambda x: float(x))
	eryvals = [sum(data['erdos-renyi'][xval])/float(len(data['erdos-renyi'][xval])) for xval in erxvals]

	baxvals = [float(x) for x in baxvals]
	eraxvals = [float(x) for x in erxvals]

	plt.plot(baxvals, bayvals)
	plt.plot(erxvals, eryvals)

	plt.xlabel(xlabel)
	plt.ylabel(ylabel)#, rotation='horizontal')
	plt.legend(['Barabasi-Albert', 'Erdos-Renyi'], loc=7)
	plt.savefig('img/' + graph + '.pdf')
	plt.clf()

def histogram(key, label, file):
	yvals = [d[key] for d in data]
	labels = [d['title'] for d in data]
	for i,y in enumerate(yvals):
		plt.bar(i, y, 0.75, facecolor=colors[i], alpha=0.75)

	plt.ylabel(label)
	plt.legend(labels)
	plt.gca().xaxis.set_major_locator(plt.NullLocator())

	plt.savefig('img/' + file + '.pdf')
	plt.clf()

def distribution(key, label, file):
	labels = [d['title'] for d in data]

	for d,color in zip(data, colors):
		degrees = [n[key] for n in d['metrics']]
		max_deg = float(max(degrees))
		x = sorted([d/max_deg for d in degrees], reverse=True)

		plt.plot(np.arange(len(x)), x, color)

	plt.ylabel(label)
	plt.legend(labels)

	plt.savefig('img/' + file + '.pdf')
	plt.clf()

robust = data['robustness']

twoplot(robust['phi'], "Size of Giant Component", "$\\phi$", "phi-robust")
plot(robust['edges']['erdos-renyi'], "Size of Giant Component", "p", "er-robust")
plot(robust['edges']['barabasi-albert'], "Size of Giant Component", "c", "ba-robust")

si = data['SI']

twoplot(si['beta'], "Time to Infection", "$\\beta$", "beta-si")
plot(si['edges']['erdos-renyi'], "Time to Infection", "p", "er-si")
plot(si['edges']['barabasi-albert'], "Time to Infection", "c", "ba-si")

