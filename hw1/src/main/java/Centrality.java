package org.gephi.toolkit.demos;

import java.io.*;
import java.util.*;
import java.util.concurrent.TimeUnit;

import org.gephi.graph.api.*;
import org.gephi.project.api.ProjectController;
import org.gephi.project.api.Workspace;
import org.openide.util.Lookup;

import org.gephi.io.importer.api.*;
import org.gephi.io.processor.plugin.DefaultProcessor;
import org.gephi.io.importer.spi .*;

import org.gephi.statistics.plugin.*;
import org.gephi.data.attributes.api.*;
import org.gephi.io.exporter.api.*;
import org.gephi.statistics.plugin.GraphDistance;

import org.gephi.statistics.plugin.EigenvectorCentrality;

import org.gephi.layout.*;
import org.gephi.layout.plugin.*;
import org.gephi.layout.plugin.forceAtlas2.*;

import org.json.JSONWriter;

public class Centrality{

  public Centrality(){

  }

  public static void main(String args[]){

    //Gephi initialization
    ProjectController pc = Lookup.getDefault().lookup(ProjectController.class);
    pc.newProject();
    Workspace workspace = pc.getCurrentWorkspace();

    GraphModel graphModel = Lookup.getDefault().lookup(GraphController.class).getModel();
    AttributeModel attributeModel = Lookup.getDefault().lookup(AttributeController.class).getModel();

    //Get controllers and models
    ImportController importController = Lookup.getDefault().lookup(ImportController.class);

    //Import file
    Container container;
    try {
      File file = new File(args[0]);
      container = importController.importFile(file);
      container.getLoader().setEdgeDefault(EdgeDefault.UNDIRECTED);   //Force UNDIRECTED
    } catch (Exception ex) {
      ex.printStackTrace();
      return;
    }

    FileWriter jsonfile;
    try {
      jsonfile = new FileWriter(new File(args[1] + ".json"));
    } catch (Exception ex) {
      ex.printStackTrace();
      return;
    }
    JSONWriter json = new JSONWriter(jsonfile);

    json.object();
    json.key("title").value(args[1]);

    //Append imported data to GraphAPI
    importController.process(container, new DefaultProcessor(), workspace);


    //See if graph is well imported
    UndirectedGraph graph = graphModel.getUndirectedGraph();

    json.key("nodes").value((long) graph.getNodeCount());
    json.key("edges").value((long) graph.getEdgeCount());

    //Create an object to calculate the EigenvectorCentrality
    EigenvectorCentrality eigenvectorCentrality = new EigenvectorCentrality();

    //calculate the eigenvector centrality
    eigenvectorCentrality.execute(graphModel,attributeModel);

    //calculate the betweeness and closeness centralities
    GraphDistance graphDistance = new GraphDistance();

    graphDistance.execute(graphModel,attributeModel);

    long degrees = 0;
    json.key("metrics").array();
    for(Node node : graph.getNodes()) {
      json.object();
      json.key("node").value(node.getNodeData().getLabel());
      json.key("betweenness").value((double)node.getAttributes().getValue("Betweenness Centrality"));
      json.key("closeness").value((double)node.getAttributes().getValue("Closeness Centrality"));
      json.key("eigenvector").value((double)node.getAttributes().getValue("Eigenvector Centrality"));
      json.key("degree").value((long) graph.getDegree(node));		

      degrees += graph.getDegree(node);
      json.endObject();
    }
    json.endArray();

    json.key("avg_degree").value(((double)degrees)/graph.getNodeCount());

    json.endObject();
    try {
      jsonfile.close();
    } catch (Exception ex) {
      ex.printStackTrace();
      return;
    }

    forceAtlas(graphModel);

    //export the graph
    exportGraph(graph, args[1]);

    exportPDF(args[1]);


  }

  static void forceAtlas(GraphModel graphModel) {
    ForceAtlas2 layout = new ForceAtlas2Builder().buildLayout();
    layout.setGraphModel(graphModel);
    layout.initAlgo();
    layout.resetPropertiesValues();
    layout.setAdjustSizes(true);
    layout.setBarnesHutOptimize(true);
    layout.setScalingRatio(200.0);
    layout.setThreadsCount(2);

    for (int i = 0; i < 10000 && layout.canAlgo(); i++) {
      layout.goAlgo();
    }
    layout.endAlgo();
  }

  static void exportPDF(String fileName){

    ExportController ec = Lookup.getDefault().lookup(ExportController.class);

    try {

      ec.exportFile(new File(fileName+".pdf"));
    } catch (IOException ex) {
      ex.printStackTrace();
      return;
    }

  }


  static void exportGraph(Graph graph, String filename){

    try{

      ExportController ec = Lookup.getDefault().lookup(ExportController.class);
      ec.exportFile(new File(filename+".gexf"));

    }catch(IOException ex){

    }
  }
}