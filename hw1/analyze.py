import json
import sys
import matplotlib.pyplot as plt
import numpy as np

data = []
for fname in sys.argv[1:]:
	with open(fname) as jsonfile:
		data.append(json.load(jsonfile))
	
	print("loaded " + data[-1]['title'])

colors = ['blue', 'green', 'red'] # who needs more than three colors really

def histogram(key, label, file):
	yvals = [d[key] for d in data]
	labels = [d['title'] for d in data]
	for i,y in enumerate(yvals):
		plt.bar(i, y, 0.75, facecolor=colors[i], alpha=0.75)

	plt.ylabel(label)
	plt.legend(labels)
	plt.gca().xaxis.set_major_locator(plt.NullLocator())

	plt.savefig('img/' + file + '.pdf')
	plt.clf()

def distribution(key, label, file):
	labels = [d['title'] for d in data]

	for d,color in zip(data, colors):
		degrees = [n[key] for n in d['metrics']]
		max_deg = float(max(degrees))
		x = sorted([d/max_deg for d in degrees], reverse=True)

		plt.plot(np.arange(len(x)), x, color)

	plt.ylabel(label)
	plt.legend(labels)

	plt.savefig('img/' + file + '.pdf')
	plt.clf()



#Number of nodes
histogram('nodes', 'Number of Nodes', 'nodes')

#Number of edges
histogram('edges', 'Number of Edges', 'edges')

#Average degree of nodes
histogram('avg_degree', 'Average Degree', 'avg-degree')

#Degree distribution
distribution('degree', 'Degree Centrality', 'degree-dist')

#Eeigenvector centrality distribution
distribution('eigenvector', 'Eigenvector Centrality', 'eigenvector-dist')

#Betweenness centrality distribution
distribution('betweenness', 'Betweenness Centrality', 'betweenness-dist')

#Closeness centrality distribution 
distribution('closeness', 'Closeness Centrality', 'closeness-dist')

# Log-Log degree with trendline

plt.yscale('log')
plt.xscale('log')

labels = []
for d,color in zip(data, colors):
	degrees = [n['degree'] for n in d['metrics']]
	max_deg = float(max(degrees))
	y = sorted([d/max_deg for d in degrees], reverse=True)
	x = np.arange(len(y)) + 1

	m,b = np.polyfit(np.log(x), np.log(y), 1)

	plt.plot(x,y, color)
	plt.plot(x, np.exp(b)*np.power(x,m), color, ls='--')

	labels.extend([d['title'], '$y = e^{' + str(b) + '}*x^{' + str(m) + '}$'])

plt.ylabel('Degree Centrality')
#plt.grid(True)
plt.legend(labels)

plt.savefig('img/log-log-deg-dist.pdf')
plt.clf()

