\documentclass[conference]{IEEEtran}

\usepackage{amsmath}
\usepackage[pdfborder={0 0 0}]{hyperref}
\usepackage{pgf}
\usepackage{tikz}
\usetikzlibrary{arrows,automata,shapes}

\newcounter{MYtempeqncnt}

\title{Modeling Uncertain System Resilience with CTMCs}

\author{%
        Nathan~Jarus\\
        Department of Computer Science\\
        Missouri University of Science and Technology\\
        Email: nmjxv3@mst.edu
}


\begin{document}
\maketitle

\begin{abstract}
	In supply network systems, resilience is a measure of a system's ability to recover from failure. 
	It captures a quantitative view of both component failure and repair, giving a view of how well a system is able to supply the demand on it. 
	Previous work has developed resilience metrics that are specialized to one failure being studied. 
	In this work, we present a system-wide resilience metric capable of expressing the effect of failure and recovery anywhere in the system.
	The technique is applied to several simple systems as well as to the IEEE 9-bus test power grid. 
\end{abstract}

\section{Introduction}
% TODO something about supply networks
Repeated cascading failures in power grids indicate a need to understand how infrastructure designs are affected by component failure. 
In 2003, blackouts across both North America~\cite{03A14Rp} and Italy~\cite{Ber04} were triggered by failure of a few power lines, which caused other lines to become overloaded and fail. 
Both blackouts left over 50 million people without power for hours as service as restored. 
Despite detailed analysis of causes behind these and other blackouts~\cite{AnD05,LiT09}, large cascading failures still occur. 
The August 2011 power blackout in San Diego was caused by circumstances very similar to the 2003 North America blackout~\cite{11S08Rp}. 
Even more recently, power failures in India in 2012~\cite{TaB12} and Turkey in 2015~\cite{15M31Rp} underscore the importance of designing systems that can reduce the likelyhood of cascading failure and recover quickly in the event of a failure. 

In any sufficiently complex system, including real-world power grids, failure is eventually inevitable. 
Resilience, a measure of the ability of a system to bounce back from failure~\cite{}, is a dependability metric that can describe the performance of a system before, during, and after failure. 
It differs from metrics such as reliability and survivability in that it can describe system behavior even after failure. 
Unlike availiability, which provides a binary view of system operation (either `functional' or `failed'), resilience offers a more detailed view of system operation. 
This view captures both normal operation, degraded performance due to partial failures, complete failure, and restored service, possibly with less excess capacity than before failure. 

Resilience measurements are based on a figure of merit for the system being measured.
Usually these figures of merit measure an attribute of one component that is representative of system performance as a whole~\cite{}. 
The chosen figure of merit is often only applicable for the specific failure cases and recovery actions being studied. 
This approach presents two problems that are especially apparent when applying it to large systems. 
First, studying the resilience of a system under a variety of partial failures will necessitate choosing different figures of merit for each test. 
These localized figures of merit cannot give an accurate representation of overall system resilience when comparing the effects of different failures.
Secondly, resilience figures cannot necessarily be compared between systems for the same reason. 

This paper presents a solution to these problems by developing a methodology for calculating an overall system resilience metric. % TODO: meh
It draws from network calculus, building a composable model based on individual system component properties. 
The system is first represented functionally as a weighted graph describing how system components are connected.
Following this, the methodology describes how to convert this functional representation into a function representing system resilience over time.
The technique presented is specialized to a specific figure of merit for power grids, but can be generalized to other figures of merit or supply networks. 
Results from applying this methodology to the IEEE 9-bus test system are compared with earlier simulation-based results. 

The remainder of this paper is organized as follows. 
In section~\ref{sec:lit-review}, work related to this methodology is outlined and discussed. 
Section~\ref{sec:methodology} discusses network calculus and its limitations when applied to the problem of resilience. 
It then quantitatively describes resilience and lays out the approach used to calculate it for a complete system. 
A case study on the IEEE 9-bus system is presented in section~\ref{sec:case-study}. 
Finally, overall conclusions are drawn in section~\ref{sec:conclusion}. 


\section{Related Work}
\label{sec:lit-review}
Trivedi et.\ al describe resilience and its relationship with other dependability metrics such as availability, reliability, and performability in \cite{TrK09}.
They present several definitions of resilience and build some models that show how availability, performability, and survivability show changes in resilience as the system parameters are changed. They extend these models in \cite{GhK13} with state-space and non-state-space models of reliability for an emergency cooling system for a boiling water nulcear reactor and performability and availability models for a telephone switching system. All of these models are concerned with transient system behavior as the system undergoes changes. 

Henry and Ramirez-Marquez introduce the concept of figures of merit (alternatively, measures of interest) \cite{HeR12}. 
They define resilience as the amount of service recovered after a degradation of service. 
In this model, the system begins at some initial level of functionality, experiences a disruption, spends some time in the disrupted state, then is recovered to a final state that may or may not be the same as the initial state. 
Furthermore, they quantify recovery time and cost, providing a basis for comparing recovery actions. 

Performability bears a close resemblance to resilience, but they differ in that resilience considers the ability of a system to be repaired or to recover from degradation. 
Meyer formulates performability as a combination of performance and reliability; that is, the ability of the system to continue performing a task even when its capability is reduced \cite{Mey80}. 
It also uses measures of interest to measure the capability of the modeled system. 
Meyer develops a hierarchical system model and discusses the difficulties involved in solving it. 
Smith and Trivedi develop a Markov Reward Model (MRM) for multiprocessor system performability in \cite{SmT88}. 
They observe changes in system performability as utilization is increased. 
They also develop an algorithm for solving MRMs to calculate accumulated reward. 

Min-plus algebra has been introduced in the field of network calculus for measuring the performance of networks over time. 
Burchard et.\ al.\ develop a calculus for statistical service guarantees in \cite{BuL02}. 
Instead of studying worst-case performance, as is usual with network calculus, the authors develop statisical service curves for system delay, backlog, and burstiness.
\cite{LeT00a} and \cite{LeT00} provide a useful tutorial for learning how to construct network calculus models. 
The authors discuss the different elements of network performance that can be captured in this model and explain how to model various types of components that may appear in a network. 
Li and Zhao delve deeper into min-plus algebra and some issues with the identity generally associated with it in~\cite{LiZ11}. 
They provide both a detailed summary of the algebra along with increasing the mathematical foundations of this field. 
He et.\ al.\ adapts network calculus to study the reliability of networks instead of their performance in~\cite{HeL10}.
In order to use min-plus algebra on the reliability probabilities of each component, the probability density functions are converted to the frequency domain using Laplace transformations, removing the need for complicated convolution calculations.
The authors model several basic network structures, including serial, parallel, circular, and back-up. 
These models are used to build models of more complex systems and to analyze their reliability over a finite number of hops in the network. 
Finally, the models can be transformed back to the time domain to derive system reliability over time. 
This calculus serves as an inspiration for several elements of the research presented in this paper. 

% TODO: A guide to the stochastic network calculus, perspectives on network calculus

\section{Methodology}
\label{sec:methodology}

\subsection{Network Calculus}
Network calculus studies the end-to-end delay in packet based computer networks without relying on the assumption of Poisson distributed traffic like queueing theory does.
The portion of network calculus that is relevant to this paper is the concept that individual node delays can be convolved to derive end-to-end delays for the entire system.
This convolution is accomplished using $(\min, +)$ algebra, which is analagous to our standard $(+, *)$ algebra.
Different network components can place bounds on the delay introduced at that node in the network. 

While network calculus provides a strong inspiration for the work in this paper, it is not directly applicable to the problem of resilience for two reasons. 
First, it assumes that whatever is being measured in the network is additive across the path taken. 
This is entirely reasonable for a computer network; however, it is not straightforward to directly add resilience values in a supply network. 
Network calculus also assumes a single source and destination for analysis, so it cannot easily be used to describe system-wide metrics. 

\subsection{System Resilience Metric}
Resilience describes system performance in the face of partial or complete failure and recovery from that failure. 
If we consider a system with a figure of merit $F(t)$, we expect it to begin at some initial value, decrease as the system's performance falls due to a failure, then increase and return to a steady value as the system is recovered as shown in Figure~\ref{fig:fom-wrt-time}. 
Note that we do not assume the system recovers to its initial capability; all that is necessary is that the system recovers to a point at which it can satisfy the demand on the system. 

\begin{figure}[!htb]
	\input{FOM-wrt-time}
	\caption{Figure of Merit vs.\ Time}
	\label{fig:fom-wrt-time}
\end{figure}

Quantitatively, we can describe the resilience of a system as

\begin{equation}
R(t) = \frac{F(t) - F(t_d)}{F(t_0) - F(t_d)}
	\label{eqn:resilience}
\end{equation}

where $t_0$ is the initial time when the system is fully functional and $t_d$ is the time at which the system experiences maximum degradation. 

%TODO: This results in infinite resilience at t=t_0?
%Because $F(t_d)$ represents the smallest amount of functionality the system has experienced in the interval $[t_0,t]$, we can represent it in min-plus terms:
%
%\begin{equation}
%F(t_d) = \min_{0 \leq u \leq t} \{ F(u) \}
%\label{eqn:min-degradation}
%\end{equation}

We represent the system being modeled as a weighted undirected graph. 
Edges in the graph represent links in the supply network, such as power lines in a power grid.
Edge weights are functions representing edge capacity as a function of time.
Nodes represent points at which the network's commodity is produced or consumed.
Each node has a function representing the quantity supplied or consumed as a function of time.

For the supply networks considered in this paper, the functionality of a system is directly related to its capacity.
The sole purpose of these networks is to transport some commodity from where it is produced to where it is consumed. 

System demand also plays a role in determining system behavior. 
A system with a light load is generally less likely to experience cascading failure than the same system with a heavier load.
Because demand ultimately determines how quickly a system is able to recover, any model for resilience must include load on the system. 
For the remainder of this paper, we assume that the system has sufficient supply to cover the demand, but it may lack the network capacity needed to carry the supply to the demand nodes. 

For these two reasons, our study of resilience will use total excess line capacity as its figure of merit. 
System supply and demand will play a role in our model's methods for deducing and relating the excess capacity of individual lines. 

We will place some constraints on our models; most of these constraints are common to existing power grid resilience techniques~\cite{AlJ14}.
First, resilience will be calculated for specific discrete timepoints. 
This allows us to ignore transient behavior in a system and to focus on system resilience at points where the system is stable. 
In addition, simulation or experimental data is often recorded as a series of data points at discrete times. 
Fitting these data points into a discrete-time model is straightforward. 

Second, figures of merit must be linear functions of system capability. 
This constraint is purely based on our formulation of resilience in Equation~\ref{eqn:resilience}.
Our chosen figure of merit, excess sytem capacity, is directly proportional to system capability. 

Third, we will assume that we have fully defined functions for individual line capacity, node supply, and node demand. 
Line capacity is usually fixed for a system; line failures can be represented by reducing their capacity to 0. 
Node supply is likewise simple to calculate; for power grids it is the maximum power able to be supplied by the grid generators.
It is also reasonable to assume that we know node demand; power grids can monitor this using smart meters, for instance.

Finally, nodes can either supply to the network or demand from the network. 
This is the case for traditional supply networks. 
In the case of distributed power grids where individuals both consume power from and supply power to the grid, the consumption and supply portions can be split into two nodes.
This is often how distributed grids are implemented in practice: individuals have one meter for their power consumption and a second that measures supply back to the grid. 


% How to integrate probability fcns?
% Prob. dist. of resilience?

% Single-component FOM issues: pathological examples go!

\subsection{Composable Component Models}

\begin{table}[h]
\begin{tabular}{l|l}
Function    & Value                                                \\ \hline \hline
$S_i(t)$    & Power supplied from node $i$                         \\
$D_i(t)$    & Power demand at node $i$                             \\
$C_{ij}(t)$ & Capacity of line connecting nodes $i$ and $j$        \\
$E_{ij}(t)$ & Excess capacity of line connecting nodes $i$ and $j$ \\
$L_{ij}(t)$ & Load on line connecting nodes $i$ and $j$\\
						& $ = C_{ij}(t) - E_{ij}(t)$
\end{tabular}
\caption{Notation}
\label{tbl:notation}
\end{table}

We will construct some basic system topologies and discuss how to measure resilience for each of them. 
These examples should illustrate the technique and provide guidelines for expanding it to more complex systems. 
Table~\ref{tbl:notation} shows the notation used in this and following sections. 

% Structures: Serial, two-serial, parallel, two-source one-sink, two-sink one-source
\begin{figure} [!ht]
\centering
\begin{tikzpicture}[-,>=stealth',shorten >=1pt,auto,node distance=3cm,bend angle=60,
	semithick,every node/.style={scale=0.9}]

  \node[state] (S)                {$S_1(t)$};
  \node[state] (D)  [right of=S]   {$D_2(t)$};

	\path (S) edge  node        {$C_{12}(t)$} (D);
\end{tikzpicture}
\caption{Series System}
\label{fig:series}
\end{figure}

Calculating excess capacity and thus resilience for a series system such as the one shown in Figure~\ref{fig:series} is straightforward.

\begin{equation}
	E_{12}(t) = C_{12}(t) - \min\{C_{12}(t),D_2(t)\}
	\label{eqn:series}
\end{equation}

Here we have two cases: the network has capacity sufficient to supply the demand, or the demand exceeds the capacity of the network. 
In the first case, there may be some excess capacity in the system; in the second, we assume the system provides the service it has the capacity to. 

\begin{figure} [!ht]
\centering
\begin{tikzpicture}[-,>=stealth',shorten >=1pt,auto,node distance=3cm,
	semithick,every node/.style={scale=0.9}]

  \node[state] (S)                {$S_1(t)$};
  \node[state] (D)  [right of=S]   {$D_2(t)$};

	\path (S) edge [bend left] node        {$C_{121}(t)$} (D)
	(S) edge [bend right] node 			{$C_{122}(t)$} (D)
	;

\end{tikzpicture}
\caption{Parallel System}
\label{fig:parallel}
\end{figure}

In a parallel system (Figure~\ref{fig:parallel}) it is not possible to conclude excess line capacities simply based on system topology. 
Fortunately, it is still possible to calculate the sum of excess line capacities, which is the value we need for resilience calculations:

\begin{equation}
\begin{split}
	E_{121}(t) + E_{122}(t) = C_{121}(t) + C_{122}(t) \\
	- \min\{C_{121}(t) + C_{122}(t),D_2(t)\}
	\label{eqn:parallel}
\end{split}
\end{equation}

We could solve for individual excess capacities by placing additional constraints on the excess capacity value, such as proportionate balancing:

\begin{equation}
	\frac{E_{121}(t)}{C_{121}(t)} = \frac{E_{122}(t)}{C_{122}(t)}
	\label{eqn:proportional-constraint}
\end{equation}

So far, these results have been intuitive, if somewhat uninteresting. 
Let's make things a bit more difficult by adding a third node:

\begin{figure} [!ht]
\centering
\begin{tikzpicture}[-,>=stealth',shorten >=1pt,auto,node distance=3cm,bend angle=60,
	semithick,every node/.style={scale=0.9}]

  \node[state] (S1)                {$S_1(t)$};
  \node[state] (D2)  [right of=S]   {$D_2(t)$};
  \node[state] (D3)  [right of=D2]   {$D_3(t)$};

	\path (S1) edge  node        {$C_{12}(t)$} (D2)
	(D2) edge node {$C_{23}(t)$} (D3)
	;
\end{tikzpicture}
\caption{Series Chain System}
\label{fig:series-chain}
\end{figure}

\begin{align}
	E_{12}(t) &= C_{12}(t) - \min\{C_{12}(t), D_2(t) + L_{23}(t)\} \\
	E_{23}(t) &= C_{23}(t) - \min\{C_{23}(t),D_3(t)\}
\end{align}

Note that both excess link capacities can be expressed based only on edges incident on the nodes connected by the link. 

Solving $E_{12}(t)$ in terms of $E_{23}(t)$ gives us an expected result:

\begin{align}
		E_{12}(t) &= C_{12}(t) - \min\{C_{12}(t), D_2(t) + \min\{C_{23}(t),D_3(t)\}\} \\
		&= C_{12}(t) - \min\{C_{12}(t),D_2(t) + C_{23}(t),D_2(t) + D_3(t)\}
\end{align}

So far, all the system topologies have featured linear topologies. 
Adding loops requires some additional intricacies. 
Redundant links between two demand nodes no longer have a fixed direction of flow, introducing multiple cases into the system for each flow direction.

\begin{figure}
\centering
\begin{tikzpicture}[-,>=stealth',shorten >=1pt,auto,node distance=3cm,bend angle=60,
	semithick,every node/.style={scale=0.9}]

  \node[state] (S1)                {$S_1(t)$};
  \node[state] (D2)  [node distance=4.5cm, right of=S ]   {$D_2(t)$};
  \node[state] (D3)  [below right of=S1]   {$D_3(t)$};

	\path (S1) edge  node        {$C_{12}(t)$} (D2)
	(D2) edge node {$C_{23}(t)$} (D3)
	(S1) edge node {$C_{13}(t)$} (D3)
	;
\end{tikzpicture}
\caption{Loop System}
\label{fig:loop}
\end{figure}

\begin{equation}
\begin{split}
	&E_{12}(t) = \\
	&\begin{cases}
		C_{12}(t) - \min\{C_{12}(t), D_2(t) + L_{23}(t)\}, & L_{12}(t) > D_2(t) \\
		C_{12}(t) - \min\{C_{12}(t), D_2(t) - L_{23}(t)\}, & L_{12}(t) \leq D_2(t)
	\end{cases}
\end{split}
\end{equation}
\begin{equation}
\begin{split}
	&E_{13}(t) = \\
	&\begin{cases}
		C_{13}(t) - \min\{C_{13}(t), D_3(t) + L_{23}(t)\}, & L_{13}(t) > D_3(t) \\
		C_{13}(t) - \min\{C_{13}(t), D_3(t) - L_{23}(t)\}, & L_{13}(t) \leq D_3(t)
	\end{cases}
\end{split}
\end{equation}
\begin{equation}
	\begin{split}
	E_{23}(t) &= C_{23}(t) - \\
	&\min\{C_{23}(t), \max\{D_2(t) - L_{12}(t), D_3(t) - L_{13}(t)\}\}
\end{split}
\end{equation}

Because we cannot load the system with more than we demand from it, we can constrain this system to eliminate the two impossible cases:

\begin{equation}
	L_{12}(t) + L_{13}(t) \leq D_2(t) + D_3(t)
\end{equation}

The two remaining cases are:
\begin{equation}
	L_{12}(t) > D_2(t) \mathrm{~and~} L_{13}(t) \leq D_3(t)
	\label{eqn:case1}
\end{equation}

 and

\begin{equation}
	L_{12}(t) \leq D_2(t) \mathrm{~and~} L_{13}(t) > D_3(t)
\end{equation}


\section{Case Study}
\label{sec:case-study}


\begin{figure}
	\includegraphics[width=0.5\textwidth]{IEEE_9}
	\caption{IEEE 9-Bus Functional System}
	\label{fig:ieee9fcnal}
\end{figure}

\begin{figure} [tb]
\centering
\begin{tikzpicture}[-,>=stealth',shorten >=1pt,auto,node distance=2.4cm,bend angle=60,
	semithick,every node/.style={scale=0.75}]

  \node[state] (S2)                {$S_2(t)$};
  \node[state] (D7)  [right of=S2]   {$D_2(t)$};
  \node[state] (D8)  [right of=D7]   {$D_8(t)$};
	\node[state] (D9)  [right of=D8]  {$D_9(t)$};
	\node[state] (S3)  [right of=D9]  {$S_3(t)$};
	\node[state] (D5)  [below of=D7]  {$D_5(t)$};
	\node[state] (D4)  [right of=D5]  {$D_4(t)$};
	\node[state] (D6)  [right of=D4]  {$D_6(t)$};
	\node[state] (S1)  [below of=D4]  {$S_1(t)$};

	\path (S2) edge  node        {$C_{27}(t)$} (D7)
	(D7) edge node {$C_{78}(t)$} (D8)
	(D8) edge node {$C_{89}(t)$} (D9)
	(D9) edge node {$C_{39}(t)$} (S3)
	(D7) edge node {$C_{57}(t)$} (D5)
	(D9) edge node {$C_{69}(t)$} (D6)
	(D5) edge node {$C_{45}(t)$} (D4)
	(D4) edge node {$C_{46}(t)$} (D6)
	(D4) edge node {$C_{14}(t)$} (S1)
	;
\end{tikzpicture}
\caption{IEEE 9-Bus Resilience Model}
\label{fig:ieee9resil}
\end{figure}

Figure~\ref{fig:ieee9fcnal} shows the IEEE 9-bus system. 
We have chosen this system for a case study in resilience modeling because we already have some results for specific failure cases in this system~\cite{AlJ14}.

We can define demand and supply functions:
\begin{align}
S_1(t) &= 72 \\
S_2(t) &= 85 \\
S_3(t) &= 163 \\
D_4(t) &= 0 \\
D_5(t) &= 125 \\
D_6(t) &= 90 \\
D_7(t) &= 0 \\
D_8(t) &= 100 \\
D_9(t) &= 0
\end{align}

In total we supply 320 MW of power and demand 315 MW, so our assumptions about the system hold. 

\begin{figure*}[!t]
% ensure that we have normalsize text
\normalsize
% Store the current equation number.
\setcounter{MYtempeqncnt}{\value{equation}}
% Set the equation number to one less than the one
% desired for the first equation here.
% The value here will have to changed if equations
% are added or removed prior to the place these
% equations are referenced in the main text.
\setcounter{equation}{23}
\begin{align}
	E_{27}(t) &= 
	\begin{cases}
		C_{27}(t) - \min\{C_{27}(t), L_{57}(t) + L_{78}(t)\},& D_8(t) > L_{89}(t), D_5(t) > L_{45}(t) \\
		C_{27}(t) - \min\{C_{27}(t), - L_{57}(t) + L_{78}(t)\},& D_8(t) > L_{89}(t), D_5(t) \leq L_{45}(t) \\
		C_{27}(t) - \min\{C_{27}(t), L_{57}(t) - L_{78}(t)\},& D_8(t) \leq L_{89}(t), D_5(t) > L_{45}(t)
	\end{cases} 
	\label{eqn:l27} \\
	E_{78}(t) &= 
	\begin{cases}
		C_{78}(t) - \min\{C_{78}(t), D_8(t) - L_{89}(t)\}, & L_{89}(t) > D_8(t) \\
		C_{78}(t) - \min\{C_{78}(t), D_8(t) + L_{89}(t)\}, & L_{89}(t) > D_8(t) \\ %TODO
		C_{78}(t) - \min\{C_{78}(t), - D_8(t) + L_{89}(t)\}, & L_{89}(t) > D_8(t)  % TODO
	\end{cases} 
	\label{eqn:l78}
\end{align}
		
% Restore the current equation number.
%\setcounter{equation}{\value{MYtempeqncnt}}
% IEEE uses as a separator
\hrulefill
% The spacer can be tweaked to stop underfull vboxes.
\vspace*{4pt}
\end{figure*}


Although the diagram does not include link capacities, we can solve for them at $t_0$:
\begin{align}
	\frac{E_{27}(t_0)}{C_{27}(t_0)} &= 1 - 0.99 \\
	\frac{E_{78}(t_0)}{C_{78}(t_0)} &= 1 - 0.79 \\
	& \vdots
\end{align}

Finally, we can set up equations describing excess link capacities. 
Equations~\ref{eqn:l27} and~\ref{eqn:l78} are representative of equations for all the lines in the system. 
Note that $D_7(t)$ is always 0, which allows us to eliminate some symbols from the equations as well as eliminating one of the four cases.

Once the equations for the system are constructed, we can use them to calculate resilience in the face of various system failures.
For instance, table~\ref{tbl:capacity} show how capacity functions can be defined to capture the failure of lines 2-7, 5-7, and 7-8. 
These lines are then recovered in the order 7-8, 5-7, 2-7. 

\begin{table}[h]
\centering
\begin{tabular}{l|lllll}
            & $t_0$         & $t_d$ & $t_1$ & $t_2$ & $t_f$ \\ \hline \hline
$C_{27}(t)$ & $C_{27}(t_0)$ & 0     & 0     & 0     &  $C_{27}(t_0)$     \\
$C_{57}(t)$ & $C_{57}(t_0)$ & 0     & 0     &  $C_{57}(t_0)$     &  $C_{57}(t_0)$     \\
$C_{78}(t)$ & $C_{78}(t_0)$ & 0     &  $C_{78}(t_0)$     &  $C_{78}(t_0)$     &   $C_{78}(t_0)$   
\end{tabular}
\caption{Capacity Functions for Failure of Lines 2-7, 5-7, and 7-8}
\label{tbl:capacity}
\end{table}

Based on these figures, system resilience can be used to compare the effects of various failures on the system and to determine which recovery methods result in a higher resilience at each time step. 

\section{Future Work and Conclusion}
\label{sec:conclusion}

This technique exposes several avenues for future work. 
First, most complex systems result in a large number of cases for each equation, which makes reasoning about and evaluating resilience difficult. 
It may be possible to modify how the system is modeled to cut down on the explosion of cases. 
Elimintating the discontinuities in the system will also make writing system solvers more tractable. 

Integrating the probability of different component failures and the rate of repair for those components could allow for a system-wide, failure-wide resilience metric.
This would allow designers to see the effect of changing one system parameter such as capacity of a link or placement of a cyber control device.
One method for including such probabilities would be representing each component with a Markov Chain model. 
However, with this approach, properly representing the conditional dependence between components that causes cascading failure would be a challenge. 

Finally, a drawback of the current modeling approach is that it requires complete knowledge of the system. 
In practice, fully characterizing a system may not be feasible due to the amount of monitoring hardware required. 
As well, each component introduces additional computational complexity. 
Being able to bound system resilience in the face of imperfect system knolwedge could allow this technique to be applied to very large systems, albeit with reduced precision.

Overall, this paper presents the beginnings of a system-wide resilience measurement technique. 
Previous resilience formulations have been made specific to one failure case being studied. 
The technique presented is both straightforward to draw from a functional system description and capable of capturing the effects of a failure anywhere in the system.

\bibliographystyle{ieeetr}
\bibliography{Mybib}

\end{document}
